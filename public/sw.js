var cacheName = 'green-quest';
var filesToCache = [
    '/',
    '/img/Arbre_1.png',
    '/img/Avatar.png',
    '/img/Logo.png',
    '/img/Logo_GQ.png',
    '/img/Arbre_2.png'
];

self.addEventListener('install', function (e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
        caches.open(cacheName).then(function (cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(filesToCache);
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request, { ignoreSearch: true }).then(response => {
            return response || fetch(event.request);
        })
    );
});