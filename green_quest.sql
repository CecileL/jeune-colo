-- MySQL Script generated by MySQL Workbench
-- jeu. 10 oct. 2019 10:31:58 CEST
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema green_quest
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema green_quest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `green_quest` DEFAULT CHARACTER SET utf8 ;
USE `green_quest` ;

-- -----------------------------------------------------
-- Table `green_quest`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `green_quest`.`users` (
  `idusers` INT NOT NULL AUTO_INCREMENT,
  `pseudo` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `motdepasse` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idusers`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `green_quest`.`defis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `green_quest`.`defis` (
  `iddefis` INT NOT NULL AUTO_INCREMENT,
  `titre` VARCHAR(45) NOT NULL,
  `resume` VARCHAR(255) NULL,
  `users_idusers` INT NOT NULL,
  PRIMARY KEY (`iddefis`, `users_idusers`),
  INDEX `fk_defis_users1_idx` (`users_idusers` ASC),
  CONSTRAINT `fk_defis_users1`
    FOREIGN KEY (`users_idusers`)
    REFERENCES `green_quest`.`users` (`idusers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `green_quest`.`conseils`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `green_quest`.`conseils` (
  `idconseils` INT NOT NULL AUTO_INCREMENT,
  `contenu` VARCHAR(255) NOT NULL,
  `users_idusers` INT NOT NULL,
  `defis_iddefis` INT NOT NULL,
  PRIMARY KEY (`idconseils`, `users_idusers`, `defis_iddefis`),
  INDEX `fk_conseils_users_idx` (`users_idusers` ASC),
  INDEX `fk_conseils_defis1_idx` (`defis_iddefis` ASC),
  CONSTRAINT `fk_conseils_users`
    FOREIGN KEY (`users_idusers`)
    REFERENCES `green_quest`.`users` (`idusers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_conseils_defis1`
    FOREIGN KEY (`defis_iddefis`)
    REFERENCES `green_quest`.`defis` (`iddefis`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
