const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt-nodejs");
const cookieParser = require("cookie-parser");
const session = require("express-session");

const app = express();

app.use(bodyParser.json());
app.use(session({secret:'petitsecret'}));
var urlencodeparser = bodyParser.urlencoded({extended: true});
app.use(urlencodeparser);
app.use(cookieParser());


/*Connexion à la base de données*/

const db = mysql.createConnection( {
    host: '127.0.0.1',
    user: 'root',
    password: 'Y.@nnick<3',
    database: 'green_quest'
});

db.connect(function (err) {
    if (err) throw err;
    console.log("La base de données est bien connectée");
});


app.set('view engine', 'ejs');

let sess;

//Création des chemins

app.get("/", function (req, res) {
    res.render('index');
});

app.get("/rejoindre", function (req, res) {
    res.render('rejoindre');
});

app.get("/connexion", function (req, res) {
    res.render('connexion');
});

app.get("/menu", function(req, res) {
    res.render('menu', {
        titre: sess.titre,
        resume: sess.resume
    });
})

app.get("/compte", function (req, res) {
    res.render('compte', {
        pseudo: sess.pseudo
    });
});

app.get("/ajout", function (req, res) {
    res.render("ajout");
});

app.get("/defi", function (req, res) {
    res.render("defi");
});

//Fin des chemins


app.use(express.static("public"));


app.post("/form/create", urlencodeparser, function (req, res) {
    //Verification de l'adresse email
    var select = "SELECT pseudo, email, motdepasse FROM users WHERE email = (?);";
    var email = [req.body.email];
    select = mysql.format(select, email);
    db.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Cette adresse mail est déjà utilisée");
        }
    });
    //Ajout d'un nouveau compte dans la BDD
    var sql = "INSERT INTO users (pseudo, email, motdepasse) VALUES (?, ?, ?);";
    var values = [req.body.pseudo, req.body.email, bcrypt.hashSync(req.body.motdepasse, null, null)];
    sql = mysql.format(sql, values);
    sess = req.session;
    sess.pseudo = req.body.pseudo;
    console.log(sess.pseudo);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            console.log("Utilisateur ajouté");
            res.redirect("/menu");
        } else {
            console.log(error);
            res.send(error);
        }
    });
});

//Se connecter avec un compte de la BDD

app.post("/form/connect", urlencodeparser, function (req, res) {
    var sql = "SELECT pseudo, email, motdepasse, idusers FROM users WHERE pseudo = (?);";
    var values = [req.body.pseudo];
    sql = mysql.format(sql, values);
    sess = req.session;
    sess.pseudo = req.body.pseudo;
    console.log(sess.pseudo);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            var msg = "Aucune correspondance trouvée dans la base de données";
            if (results.length != 1) {
                console.log(results);
                res.send(msg);
            } else {
                if (bcrypt.compareSync(req.body.motdepasse, results[0].motdepasse)) {
                    logged_user = {
                        pseudo: results[0].pseudo,
                        email: results[0].email,
                        idusers: results[0].idusers
                    };
                    console.log(logged_user);
                    message = "Vous êtes bien connecté";
                    console.log("Utilisateur connecté");
                    res.redirect("/menu");
                } else {
                    res.send(msg);
                }
            }
        } else {
            res.send(error);
        }
    });
});


//Ajouter un nouveau défi

app.post("/form/ajout", urlencodeparser, function (req, res) {
    var sql = "INSERT INTO defis (titre, resume, users_idusers) VALUES (?,?,?);";
    var values = [req.body.titre, req.body.resume, logged_user.idusers];
    sql = mysql.format(sql, values);
    sess = req.session;
    sess.titre = req.body.titre;
    sess.resume = req.body.resume;
    console.log(sess.titre);
    console.log(sess.resume);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            console.log("Le défis a bien été ajouté")
            res.redirect("/menu");
        } else {
            console.log(error)
            res.send(error);
        }
    });
});



app.listen(3112, function() {
    console.log('le serveur écoute sur le port 3112');
});

